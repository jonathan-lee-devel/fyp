import pygame

# Initialize pygame
pygame.init()

# Create display
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 800
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Traffic Simulator")

# Main loop
running = True
while running:
    # Event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                running = False

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_q:
                running = False

        # Display code
        screen.fill((0, 102, 0))  # Green background
        pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(0, 300, 800, 200))  # Vertical road
        pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(300, 0, 200, 800))  # Horizontal road

        # Final display update
        pygame.display.update()

# End of application
print('Application exiting...')
pygame.quit()
exit(0)
